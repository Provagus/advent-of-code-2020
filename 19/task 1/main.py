def checkCompability(text : str, ruleArray : list, rules : dict):
    fulfilled = False
    for branch in ruleArray:
        nextRule = branch[0]
        rule = rules[nextRule]
        for branchNext in rule:
            branchCopy = branch.copy()
            newBranch = []
            letters = 0
            newText = text
            for x in branchNext:
                if type(x) == str:
                    if x != text[0]:
                        return False
                    else:
                        newText = newText[1:]
                        letters += 1
                        if len(newText) == 0:
                            if len(newBranch[letters:] + branchCopy[1:]) == 0:
                                return True
                            else:
                                return False
                else:
                    newBranch.append(x)
            newBranch = newBranch[letters:] + branchCopy[1:]
            if len(newBranch) == 0 and len(newText) > 0:
                return False
            if checkCompability(newText, [newBranch], rules):
                fulfilled = True
                break
        if fulfilled:
            break
    return fulfilled



with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    ruleSection = True
    rules = {}
    sum = 0
    for x in input:
        if x == "":
            ruleSection = False
            continue
        elif ruleSection == True:
            x = x.split(": ")
            ruleNumber = int(x[0])
            rule = x[1]
            rule = rule.split(" | ")
            for i in range(0, len(rule)):
                rule[i] = rule[i].replace('"', "")
                rule[i] = rule[i].split(" ")
                for j in range(0, len(rule[i])):
                    if rule[i][j].isdigit():
                        rule[i][j] = int(rule[i][j])
            rules[ruleNumber] = rule
        else:
            if checkCompability(x, rules[0], rules):
                sum += 1
    print(sum)
