def makeMove(startingOrder : list, currentCup : int, lowestValue : int, highestValue : int):
    pickUp = []
    removalPos = currentCup + 1
    destination = startingOrder[currentCup]
    currentCup = startingOrder[currentCup]
    for i in range(0, 3):
        if removalPos < len(startingOrder):
            pickUp.append(startingOrder[removalPos])
            startingOrder.pop(removalPos)
        else:
            removalPos = 0
            pickUp.append(startingOrder[removalPos])
            startingOrder.pop(removalPos)
    while True:
        destination -= 1
        if destination < lowestValue:
            destination = highestValue
        if destination in startingOrder:
            destinationIndex = startingOrder.index(destination)
            for j in reversed(range(0, 3)):
                startingOrder.insert(destinationIndex+1, pickUp[j])
            break
    currentCup = startingOrder.index(currentCup)
    currentCup += 1
    if currentCup >= len(startingOrder):
        currentCup = 0
    return currentCup

with open("input.in", "r") as file:
    input = file.read()
    currentCupOrder = list(input)
    for i in range(0, len(currentCupOrder)):
        currentCupOrder[i] = int(currentCupOrder[i])
    lowestValue = min(currentCupOrder)
    highestValue = max(currentCupOrder)
    cupPos = 0
    for i in range(0, 100):
        cupPos = makeMove(currentCupOrder, cupPos, lowestValue, highestValue)
    positionOf1 = currentCupOrder.index(1)
    currentCupOrder = currentCupOrder[positionOf1:] + currentCupOrder[:positionOf1]
    asnwerArray = currentCupOrder[1:]
    answer = ""
    for x in asnwerArray:
        answer += str(x)
    print(answer)