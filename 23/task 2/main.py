def makeMove(previousCup : list, nextCup : list, currentCup : int, lowestValue : int, highestValue : int):
    pickUp = []
    destination = currentCup

    pickUp.append(nextCup[currentCup])
    pickUp.append(nextCup[nextCup[currentCup]])
    pickUp.append(nextCup[nextCup[nextCup[currentCup]]])
    previousCup[nextCup[nextCup[nextCup[nextCup[currentCup]]]]] = currentCup
    nextCup[currentCup] = nextCup[nextCup[nextCup[nextCup[currentCup]]]]

    while True:
        destination -= 1
        if destination < lowestValue:
            destination = highestValue
        if destination in pickUp:
            continue
        nextAfterDestination = nextCup[destination]
        previousCup[nextAfterDestination] = pickUp[2]
        nextCup[destination] = pickUp[0]
        nextCup[pickUp[2]] = nextAfterDestination
        previousCup[pickUp[0]] = destination
        break
    currentCup = nextCup[currentCup]
    return currentCup

with open("input.in", "r") as file:
    input = file.read()
    currentCupOrder = list(input)

    for i in range(0, len(currentCupOrder)):
        currentCupOrder[i] = int(currentCupOrder[i])
    lowestValue = min(currentCupOrder)
    highestValue = max(currentCupOrder)
    
    for i in range(highestValue+1, 1000001):
        currentCupOrder.append(i)
    
    nextCup = [0] * (len(currentCupOrder) + 1)
    previousCup = [0] * (len(currentCupOrder) + 1)
    
    previous = 0
    for x in currentCupOrder:
        previousCup[x] = previous
        previous = x
    previousCup[currentCupOrder[0]] = previous

    next = 0
    for x in reversed(currentCupOrder):
        nextCup[x] = next
        next = x
    nextCup[currentCupOrder[-1]] = next

    highestValue = max(currentCupOrder)
    cupPos = currentCupOrder[0]
    i = 0
    lengthOfArray = len(currentCupOrder)
    for i in range(0, 10000000):
        cupPos = makeMove(previousCup, nextCup, cupPos, lowestValue, highestValue)

    result = nextCup[1] * nextCup[nextCup[1]]
    print(result)