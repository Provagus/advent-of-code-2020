with open("input.in", "r") as file:
    input = file.read()
    input = input.split(",")
    occurences = {}
    next = 0
    for (x, i) in zip(input, range(1, len(input) + 1)):
        x = int(x)
        if x in occurences:
            tmp = occurences[x]
            occurences[x] = i
            next = i - tmp
        else:
            next = 0
        occurences[x] = i
    for i in range(len(input)+1, 30000000):
        if next in occurences:
            tmp = occurences[next]
            occurences[next] = i
            next = i - tmp
        else:
            occurences[next] = i
            next = 0
    print(next)