def removeOccupiedAllergens(allergens : dict, allergen : str, ingredient : str):
    for x in allergens:
        if x == allergen:
            continue
        if ingredient in allergens[x]:
            allergens[x].remove(ingredient)
            if len(allergens[x]) == 1:
                removeOccupiedAllergens(allergens, x, allergens[x][0])

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    allergens = {}
    for line in input:
        line = line.split(" (contains ")
        line[1] = line[1].replace(")", "")
        line[1] = line[1].split(", ")
        line[0] = line[0].split(" ")
        for allergen in line[1]:
            if allergen in allergens:
                newArray = []
                for ingredient in line[0]:
                    if ingredient in allergens[allergen]:
                        newArray.append(ingredient)
                allergens[allergen] = newArray
            else:
                allergens[allergen] = line[0]
    singleIngrediens = []
    for allergen in allergens:
        if len(allergens[allergen]) == 1:
            singleIngrediens.append({
                "ingredient" : allergens[allergen][0],
                "allergen" : allergen
                })
    for x in singleIngrediens:
        removeOccupiedAllergens(allergens, x["allergen"], x["ingredient"])
    ingredientsWithAllergens = []
    for x in allergens:
        ingredientsWithAllergens.append(allergens[x][0])
    answer = 0
    for line in input:
        line = line.split(" (contains ")
        line[0] = line[0].split(" ")
        for x in line[0]:
            if not x in ingredientsWithAllergens:
                answer += 1
    print(answer)