def removeOccupiedAllergens(allergens : dict, allergen : str, ingredient : str):
    for x in allergens:
        if x == allergen:
            continue
        if ingredient in allergens[x]:
            allergens[x].remove(ingredient)
            if len(allergens[x]) == 1:
                removeOccupiedAllergens(allergens, x, allergens[x][0])

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    allergens = {}
    for line in input:
        line = line.split(" (contains ")
        line[1] = line[1].replace(")", "")
        line[1] = line[1].split(", ")
        line[0] = line[0].split(" ")
        for allergen in line[1]:
            if allergen in allergens:
                newArray = []
                for ingredient in line[0]:
                    if ingredient in allergens[allergen]:
                        newArray.append(ingredient)
                allergens[allergen] = newArray
            else:
                allergens[allergen] = line[0]
    singleIngrediens = []
    for allergen in allergens:
        if len(allergens[allergen]) == 1:
            singleIngrediens.append({
                "ingredient" : allergens[allergen][0],
                "allergen" : allergen
                })
    for x in singleIngrediens:
        removeOccupiedAllergens(allergens, x["allergen"], x["ingredient"])
    finalAssigments = []
    for x in allergens:
        allergen = x
        ingredient = allergens[x][0]
        finalAssigments.append({
            "allergen" : allergen,
            "ingredient" : ingredient
        })
    finalAssigments.sort(key= lambda x: x["allergen"])
    answer = ""
    for x in finalAssigments:
        answer += f"{x['ingredient']},"
    answer = answer[:-1]
    print(answer)