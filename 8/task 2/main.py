with open("input.in", "r") as file:
    oldCode = file.read()
    oldCode = oldCode.split("\n")
    codeLen = len(oldCode)
    bruteForceCounter = 0
    while True:
        acc = 0
        pc = 0
        visited = [False] * len(oldCode)
        code = oldCode.copy()
        encountered = 0
        replaced = False
        for (x, i) in zip(oldCode, range(0, codeLen)):
            instruction = x.split(" ")[0]
            if instruction == "nop" or instruction == "jmp":
                if encountered == bruteForceCounter:
                    if instruction == "nop":
                        code[i] = code[i].replace("nop", "jmp")
                    else:
                        code[i] = code[i].replace("jmp", "nop")
                    replaced = True
                    break
                encountered += 1
        if not replaced:
            break
        while True:
            if pc >= codeLen or visited[pc]:
                break
            lastInstruction = False
            if pc == codeLen - 1:
                lastInstruction = True
            visited[pc] = True
            instruction = code[pc].split(" ")
            if instruction[0] == "acc":
                acc += int(instruction[1])
            elif instruction[0] == "jmp":
                pc += int(instruction[1])
                if lastInstruction:
                    print(acc)
                continue
            pc += 1
            if lastInstruction:
                print(acc)
        bruteForceCounter += 1
