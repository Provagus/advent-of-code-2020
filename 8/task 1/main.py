with open("input.in", "r") as file:
    code = file.read()
    code = code.split("\n")
    acc = 0
    pc = 0
    visited = [False] * len(code)
    while True:
        if visited[pc]:
            print(acc)
            break
        visited[pc] = True
        instruction = code[pc].split(" ")
        if instruction[0] == "acc":
            acc += int(instruction[1])
        elif instruction[0] == "jmp":
            pc += int(instruction[1])
            continue
        pc += 1

