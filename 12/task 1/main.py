from os import curdir


NORTH = 0
EAST = 1
SOUTH = 2
WEST = 3

def rotate(current, left, rotation):
    if left:
        current -= rotation // 90
        current %= 4
    else:
        current += rotation // 90
        current %= 4
    return current

with open("input.in", "r") as file:
    commands = file.read()
    commands = commands.split("\n")
    rotation = EAST
    x = 0
    y = 0
    for a in commands:
        number = int(a[1:])
        if a[0] == "N":
            x += number
        elif a[0] == "S":
            x -= number
        elif a[0] == "E":
            y += number
        elif a[0] == "W":
            y -= number
        elif a[0] == "L":
            rotation = rotate(rotation, True, number)
        elif a[0] == "R":
            rotation = rotate(rotation, False, number)
        elif a[0] == "F":
            if rotation == NORTH:
                x += number
            elif rotation == SOUTH:
                x -= number
            elif rotation == EAST:
                y += number
            elif rotation == WEST:
                y -= number
    print(abs(x) + abs(y))