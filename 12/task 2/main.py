from os import curdir

def rotate(left, rotation, x, y):
    current = 0
    if left:
        current -= rotation // 90
        current %= 4
    else:
        current += rotation // 90
        current %= 4
    if current == 0:
        return x, y
    elif current == 2:
        return -x, -y
    elif current == 1:
        return -y, x
    elif current == 3:
        return y, -x

with open("input.in", "r") as file:
    commands = file.read()
    commands = commands.split("\n")
    x = 0
    y = 0
    wx = 1
    wy = 10
    for a in commands:
        number = int(a[1:])
        if a[0] == "N":
            wx += number
        elif a[0] == "S":
            wx -= number
        elif a[0] == "E":
            wy += number
        elif a[0] == "W":
            wy -= number
        elif a[0] == "L":
            wx, wy = rotate(True, number, wx, wy)
        elif a[0] == "R":
            wx, wy = rotate(False, number, wx, wy)
        elif a[0] == "F":
            x += wx * number
            y += wy * number
    print(abs(x) + abs(y))