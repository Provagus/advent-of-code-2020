REMAINDER = 20201227
SUBJECT_NUMBER = 7

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    cardKey = int(input[0])
    doorKey = int(input[1])

    startingValue = 1
    cardIterations = 0
    while startingValue != cardKey:
        startingValue *= SUBJECT_NUMBER
        startingValue %= REMAINDER
        cardIterations += 1
    
    startingValue = 1
    subjectNumber = doorKey
    for i in range(0, cardIterations):
        startingValue *= subjectNumber
        startingValue %= REMAINDER
    print(startingValue)