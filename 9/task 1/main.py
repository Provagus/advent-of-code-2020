with open("input.in", "r") as file:
    numbers = file.read()
    numbers = numbers.split("\n")
    for (number, i) in zip(numbers, range(0, len(numbers))):
        number = int(number)
        numbers[i] = number
        if i >= 25:
            found = False
            for j in range(i-25, i):
                for k in range(j+1, i):
                    if numbers[j] + numbers[k] == number:
                        found = True
                        break
                if found:
                    break
            if not found:
                print(number)
                break
