with open("input.in", "r") as file:
    numbers = file.read()
    numbers = numbers.split("\n")
    invalidNumber = None
    for (number, i) in zip(numbers, range(0, len(numbers))):
        number = int(number)
        numbers[i] = number
        if i >= 25:
            found = False
            for j in range(i-25, i):
                for k in range(j+1, i):
                    if numbers[j] + numbers[k] == number:
                        found = True
                        break
                if found:
                    break
            if not found:
                invalidNumber = number
                break
    found = False
    for i in range(0, len(numbers)):
        sum = 0
        for j in range(i, len(numbers)):
            sum += numbers[j]
            if sum == invalidNumber:
                rangeSet = numbers[i:j]
                answer = min(rangeSet) + max(rangeSet)
                print(answer)
                found = True
                break
            elif sum > invalidNumber:
                break
        if found:
            break
