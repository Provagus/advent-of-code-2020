import re

passports = None
with open("input.in", "r") as file:
    passports = file.read()

valid = 0

passports = passports.split("\n\n")
for passport in passports:
    dataForChecking = {}
    passport = re.split(" |\n", passport)
    for data in passport:
        data = data.split(":")
        dataForChecking[data[0]] = True
    if ("byr" in dataForChecking and
        "iyr" in dataForChecking and
        "eyr" in dataForChecking and
        "hgt" in dataForChecking and
        "hcl" in dataForChecking and
        "ecl" in dataForChecking and
        "pid" in dataForChecking):
        valid += 1
print(valid)