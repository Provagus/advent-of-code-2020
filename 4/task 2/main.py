import re

BYR_MIN = 1920
BYR_MAX = 2002
IYR_MIN = 2010
IYR_MAX = 2020
EYR_MIN = 2020
EYR_MAX = 2030
HGT_CM_MIN = 150
HGT_CM_MAX = 193
HGT_IN_MIN = 59
HGT_IN_MAX = 76


passports = None
with open("input.in", "r") as file:
    passports = file.read()

valid = 0

passports = passports.split("\n\n")
for passport in passports:
    correct = True
    dataForChecking = {}
    passport = re.split(" |\n", passport)
    for data in passport:
        data = data.split(":")
        field = data[0]
        value = data[1]
        dataForChecking[field] = True
        if field == "byr":
            value = int(value)
            if not BYR_MIN <= value <= BYR_MAX:
                correct = False

        elif field == "iyr":
            value = int(value)
            if not IYR_MIN <= value <= IYR_MAX:
                correct = False

        elif field == "eyr":
            value = int(value)
            if not EYR_MIN <= value <= EYR_MAX:
                correct = False

        elif field == "hgt":
            if value[-2:] == "cm":
                value = int(value[:-2])
                if not HGT_CM_MIN <= value <= HGT_CM_MAX:
                    correct = False
            elif value[-2:] == "in":
                value = int(value[:-2])
                if not HGT_IN_MIN <= value <= HGT_IN_MAX:
                    correct = False
            else:
                correct = False

        elif field == "hcl":
            if not re.match("^#[0-9a-f]{6}$", value):
                correct = False

        elif field == "ecl":
            if not re.match("^amb|blu|brn|gry|grn|hzl|oth$", value):
                correct = False

        elif field == "pid":
            if not re.match("^\d{9}$", value):
                correct = False
        if not correct:
            break

    if (correct and
        "byr" in dataForChecking and
        "iyr" in dataForChecking and
        "eyr" in dataForChecking and
        "hgt" in dataForChecking and
        "hcl" in dataForChecking and
        "ecl" in dataForChecking and
        "pid" in dataForChecking):
        valid += 1
print(valid)