valid = 0

with open("input.in", "r") as file:
    lines = file.readlines()
    for x in lines:
        x = x.split(" ")
        x[0] = x[0].split("-")
        x[1] = x[1][:-1]
        amount = x[2].count(x[1])
        if int(x[0][0]) <= amount <= int(x[0][1]):
            valid += 1
print(valid)