valid = 0

with open("input.in", "r") as file:
    lines = file.readlines()
    for x in lines:
        x = x.split(" ")
        x[0] = x[0].split("-")
        x[1] = x[1][:-1]
        pos1 = int(x[0][0]) - 1
        pos2 = int(x[0][1]) - 1
        if ((x[2][pos1] == x[1] and x[2][pos2] != x[1]) 
            or 
            (x[2][pos1] != x[1] and x[2][pos2] == x[1])):
            valid += 1
print(valid)