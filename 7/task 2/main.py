def countBags(tree, color, amount):
    result = amount
    for x in tree[color]["childs"]:
        result += countBags(tree, x["color"], amount*x["amount"])
    return result


with open("input.in", "r") as file:
    rules = file.read()
    rules = rules.split("\n")
    result = 0
    ruleSet = {}
    # string analysis
    # seeding childs
    for rule in rules:
        rule = rule.split(" bags contain ")
        color = rule[0]
        contains = {
            "childs" : []
        }
        if rule[1].find("no other bags") != -1:
            ruleSet[color] = contains
            continue
        rule[1].replace(".", "")
        inside = rule[1].split(", ")
        for x in inside:
            x = x.split(" bag")
            x = x[0]
            bagColor = x[x.find(" ")+1:]
            amount = int(x[:x.find(" ")])
            contains["childs"].append({
                "color" : bagColor,
                "amount" : amount
                })
        ruleSet[color] = contains

    print(countBags(ruleSet, "shiny gold", 1)-1)
