def iterateOverParents(tree, color):
    tree[color]["gold"] = True
    for x in tree[color]["parents"]:
        if x != "shiny gold" and tree[x]["gold"] != True:
            iterateOverParents(tree, x)

with open("input.in", "r") as file:
    rules = file.read()
    rules = rules.split("\n")
    result = 0
    ruleSet = {}
    # string analysis
    # seeding childs
    for rule in rules:
        rule = rule.split(" bags contain ")
        color = rule[0]
        contains = {
            "childs" : [],
            "parents" : [],
            "gold" : False
        }
        if rule[1].find("no other bags") != -1:
            ruleSet[color] = contains
            continue
        rule[1].replace(".", "")
        inside = rule[1].split(", ")
        for x in inside:
            x = x.split(" bag")
            x = x[0]
            x = x[x.find(" ")+1:]
            contains["childs"].append(x)
        ruleSet[color] = contains
    # Seeding parents
    for color in ruleSet:
        for x in ruleSet[color]["childs"]:
            ruleSet[x]["parents"].append(color)
    # Analyzing which bags can hold gold one
    for color in ruleSet:
        if color != "shiny gold" and ruleSet[color]["gold"] != True:
            currentlyAnalyzed = ruleSet[color]
            if "shiny gold" in currentlyAnalyzed["childs"]:
                iterateOverParents(ruleSet, color)
    for color in ruleSet:
        if ruleSet[color]["gold"] == True:
            result += 1
    print(result)
