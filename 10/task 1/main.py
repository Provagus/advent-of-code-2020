with open("input.in", "r") as file:
    numbers = file.read()
    numbers = numbers.split("\n")
    differences = [0] * (len(numbers) + 1)
    for (number, i) in zip(numbers, range(0, len(numbers))):
        number = int(number)
        numbers[i] = number
    numbers.sort()
    numbers.append(numbers[-1] + 3)
    differences[0] = numbers[0]
    for i in range(1, len(numbers)):
        differences[i] = numbers[i] - numbers[i-1]
    ones = differences.count(1)
    threes = differences.count(3)
    print(ones * threes)