with open("input.in", "r") as file:
    numbers = file.read()
    numbers = numbers.split("\n")
    for (number, i) in zip(numbers, range(0, len(numbers))):
        number = int(number)
        numbers[i] = number
    numbers.insert(0, 0)
    numbers.sort()
    numbers.append(numbers[-1] + 3)
    maxNumber = numbers[-1]
    posibilitiesSet = {}
    for x in numbers:
        posibilitiesSet[x] = 0
    posibilitiesSet[maxNumber] = 1
    for i in reversed(range(0, maxNumber)):
        if i in posibilitiesSet:
            posibilities = 0
            if i + 1 in posibilitiesSet:
                posibilities += posibilitiesSet[i+1]
            if i + 2 in posibilitiesSet:
                posibilities += posibilitiesSet[i+2]
            if i + 3 in posibilitiesSet:
                posibilities += posibilitiesSet[i+3]
            posibilitiesSet[i] = posibilities
    print(posibilitiesSet[0])