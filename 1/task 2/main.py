import os

with open("input.in", "r") as file:
    numbers = file.readlines()
    i = 0
    for x in numbers:
        i += 1
        brk = False
        num1 = int(x)
        for j in range(i, len(numbers)):
            num2 = int(numbers[j])
            for k in range(j, len(numbers)):
                num3 = int(numbers[k])
                if num1 + num2 + num3 == 2020:
                    print("Numbers: {} {} {}".format(num1, num2, num3))
                    print("Solution: {}".format(num1 * num2 * num3))
                    brk = True
                    break
            if brk:
                break
        if brk:
            break
