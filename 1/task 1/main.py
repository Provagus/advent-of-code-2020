import os

with open("input.in", "r") as file:
    numbers = file.readlines()
    i = 0
    for x in numbers:
        i += 1
        brk = False
        for j in range(i, len(numbers)):
            if int(x) + int(numbers[j]) == 2020:
                print("Numbers: {} {}".format(int(x), int(numbers[j])))
                print("Solution: {}".format(int(x) * int(numbers[j])))
                brk = True
                break
        if brk:
            break
