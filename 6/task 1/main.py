with open("input.in", "r") as file:
    answers = file.read()
    sum = 0
    answers = answers.split("\n\n")
    for group in answers:
        group = group.replace("\n", "")
        uniqueAns = set(group)
        uniqueAns = len(uniqueAns)
        sum += uniqueAns
    print(sum)