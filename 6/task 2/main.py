with open("input.in", "r") as file:
    answers = file.read()
    sum = 0
    answers = answers.split("\n\n")
    for group in answers:
        group = group.split("\n")
        groupSize = len(group)
        uniqueLetters = {}
        for person in group:
            for letter in range(0, len(person)):
                if not person[letter] in uniqueLetters:
                    uniqueLetters[person[letter]] = 1
                else:
                    uniqueLetters[person[letter]] += 1
        for letter in uniqueLetters:
            if uniqueLetters[letter] == groupSize:
                sum += 1
    print(sum)