with open("input.in", "r") as file:
    tickets = file.read()
tickets = tickets.split()

maxNumber = 0

for x in tickets:
    lowerBound = 0
    upperBound = 127
    for i in range(0, 7):
        letter = x[i]
        if letter == "F":
            upperBound = (lowerBound + upperBound) // 2
        elif letter == "B":
            lowerBound = round((lowerBound + upperBound) / 2)
    row = upperBound
    lowerBound = 0
    upperBound = 7
    for i in range(7, 10):
        letter = x[i]
        if letter == "L":
            upperBound = (lowerBound + upperBound) // 2
        elif letter == "R":
            lowerBound = round((lowerBound + upperBound) / 2)
    column = upperBound
    maxNumber = max(maxNumber, row * 8 + column)
print(maxNumber)