import sys

with open("input.in", "r") as file:
    tickets = file.read()
tickets = tickets.split()

maxNumber = 0
minNumber = sys.maxsize

seats = {}

for x in tickets:
    lowerBound = 0
    upperBound = 127
    for i in range(0, 7):
        letter = x[i]
        if letter == "F":
            upperBound = (lowerBound + upperBound) // 2
        elif letter == "B":
            lowerBound = round((lowerBound + upperBound) / 2)
    row = upperBound
    lowerBound = 0
    upperBound = 7
    for i in range(7, 10):
        letter = x[i]
        if letter == "L":
            upperBound = (lowerBound + upperBound) // 2
        elif letter == "R":
            lowerBound = round((lowerBound + upperBound) / 2)
    column = upperBound
    seatID = row * 8 + column
    maxNumber = max(maxNumber, seatID)
    minNumber = min(minNumber, seatID)
    seats[seatID] = True

for x in range(minNumber, maxNumber + 1):
    if not x in seats:
        print(x)
        break