RANGES = 0
YOUR_TICKET = 1
OTHER_TICKETS = 2

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    currently = RANGES
    fields = {}
    fieldPosibilities = []
    tickets = []
    # Input processing and eliminating invalid tickets
    for x in input:
        if x == "":
            currently += 1
            continue
        if x == "your ticket:" or x == "nearby tickets:":
            continue
        if currently == RANGES:
            x = x.split(": ")
            name = x[0]
            ranges = x[1]
            ranges = ranges.split(" or ")
            for i in range(0, len(ranges)):
                ranges[i] = ranges[i].split("-")
                ranges[i][0] = int(ranges[i][0])
                ranges[i][1] = int(ranges[i][1])
            fields[name] = ranges
        elif currently == YOUR_TICKET:
            numbers = x.split(",")
            for i in range(0, len(numbers)):
                numbers[i] = int(numbers[i])
            tickets.append(numbers)
        elif currently == OTHER_TICKETS:
            valid = True
            numbers = x.split(",")
            for i in range(0, len(numbers)):
                numbers[i] = int(numbers[i])
                inAnyRange = False
                for field in fields:
                    for rang in fields[field]:
                        if rang[0] <= numbers[i] <= rang[1]:
                            inAnyRange = True
                            break
                    if inAnyRange:
                        break
                if not inAnyRange:
                    valid = False
                    break
            if valid:
                tickets.append(numbers)
    # Filling array with arrays
    for x in tickets[0]:
        fieldPosibilities.append({})
    # Analyzis of posible fields based on ranges
    for (x, i) in zip(tickets, range(0, len(tickets))):
        for (number, j) in zip(x, range(0, len(x))):
            for field in fields:
                inRange = False
                for rang in fields[field]:
                    if rang[0] <= number <= rang[1]:
                        inRange = True
                        if i == 0:
                            fieldPosibilities[j][field] = True
                        break
                if not inRange:
                    fieldPosibilities[j].pop(field, None)
    # Assigning fields based on indexes with only one valid key
    found = 0
    numberOfFields = len(fieldPosibilities)
    while found != numberOfFields:
        found = 0
        for (x, i) in zip(fieldPosibilities, range(0, len(fieldPosibilities))):
            if len(x) == 1:
                found += 1
                x = list(x.keys())
                for (field, j) in zip(fieldPosibilities, range(0, len(fieldPosibilities))):
                    if i == j:
                        continue
                    field.pop(x[0], None)
    # Answer calculation
    answer = 1
    for i in range(0, len(fieldPosibilities)):
        field = fieldPosibilities[i]
        field = list(field.keys())[0]
        if field[:9] == "departure":
            answer *= tickets[0][i]
    print(answer)
