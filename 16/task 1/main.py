RANGES = 0
YOUR_TICKET = 1
OTHER_TICKETS = 2

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    currently = RANGES
    fields = {}
    sum = 0
    for x in input:
        if x == "":
            currently += 1
            continue
        if x == "your ticket:" or x == "nearby tickets:":
            continue
        if currently == RANGES:
            x = x.split(": ")
            name = x[0]
            ranges = x[1]
            ranges = ranges.split(" or ")
            for i in range(0, len(ranges)):
                ranges[i] = ranges[i].split("-")
                ranges[i][0] = int(ranges[i][0])
                ranges[i][1] = int(ranges[i][1])
            fields[name] = ranges
        elif currently == YOUR_TICKET:
            continue
        elif currently == OTHER_TICKETS:
            numbers = x.split(",")
            for number in numbers:
                number = int(number)
                inAnyRange = False
                for field in fields:
                    for rang in fields[field]:
                        if rang[0] <= number <= rang[1]:
                            inAnyRange = True
                            break
                    if inAnyRange:
                        break
                if not inAnyRange:
                    sum += number
    print(sum)