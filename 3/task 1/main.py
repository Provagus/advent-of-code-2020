trees = 0
currentPosition = 0

with open("input.in", "r") as file:
    lines = file.readlines()
    for x in lines:
        if x[-1] == "\n":
            x = x[:-1]
        if x[currentPosition] == "#":
            trees += 1
        currentPosition += 3
        currentPosition %= len(x)
print(trees)