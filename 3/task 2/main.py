with open("input.in", "r") as file:
    lines = file.readlines()

def findTrees(down, right):
    trees = 0
    currentPosition = 0
    i = 0
    for x in lines:
        if i % down != 0:
            i += 1
            continue
        if x[-1] == "\n":
            x = x[:-1]
        if x[currentPosition] == "#":
            trees += 1
        currentPosition += right
        currentPosition %= len(x)
        i += 1
    return trees

result = findTrees(1, 1) * findTrees(1, 3) * findTrees(1, 5) * findTrees(1, 7) * findTrees(2, 1)
print(result)