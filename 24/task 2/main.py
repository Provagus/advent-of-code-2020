def increaseAdjustment(adjSet : dict, cordX : int, cordY : int):
    pos = (cordX, cordY)
    if pos in adjSet:
        adjSet[pos] += 1
    else:
        adjSet[pos] = 1

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    tiles = {}
    for line in input:
        currentX = 0
        currentY = 0
        while line:
            if line[0] == "e":
                line = line[1:]
                currentX += 1
            elif line[0] == "w":
                line = line[1:]
                currentX -= 1
            elif line [:2] == "ne":
                line = line[2:]
                currentY += 1
            elif line [:2] == "nw":
                line = line[2:]
                currentX -= 1
                currentY += 1
            elif line [:2] == "se":
                line = line[2:]
                currentX += 1
                currentY -= 1
            elif line [:2] == "sw":
                line = line[2:]
                currentY -= 1
        position = (currentX, currentY)
        if position in tiles:
            tiles[position] = not tiles[position]
        else:
            tiles[position] = True
    for i in range(0, 100):
        adjustement = {}
        for x in tiles:
            if tiles[x]:
                increaseAdjustment(adjustement, x[0], x[1] + 1)
                increaseAdjustment(adjustement, x[0], x[1] - 1)
                increaseAdjustment(adjustement, x[0] + 1, x[1])
                increaseAdjustment(adjustement, x[0] - 1, x[1])
                increaseAdjustment(adjustement, x[0] - 1, x[1] + 1)
                increaseAdjustment(adjustement, x[0] + 1, x[1] - 1)
        for x in tiles:
            if tiles[x] and not x in adjustement:
                tiles[x] = False
        for x in adjustement:
            if adjustement[x] == 2:
                tiles[x] = True
            else:
                if x in tiles and tiles[x]:
                    if adjustement[x] > 2:
                        tiles[x] = False

    sum = 0
    for x in tiles:
        if tiles[x]:
            sum += 1
    print(sum)