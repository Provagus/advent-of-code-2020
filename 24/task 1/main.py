with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    tiles = {}
    for line in input:
        currentX = 0
        currentY = 0
        while line:
            if line[0] == "e":
                line = line[1:]
                currentX += 1
            elif line[0] == "w":
                line = line[1:]
                currentX -= 1
            elif line [:2] == "ne":
                line = line[2:]
                currentY += 1
            elif line [:2] == "nw":
                line = line[2:]
                currentX -= 1
                currentY += 1
            elif line [:2] == "se":
                line = line[2:]
                currentX += 1
                currentY -= 1
            elif line [:2] == "sw":
                line = line[2:]
                currentY -= 1
        position = (currentX, currentY)
        if position in tiles:
            tiles[position] = not tiles[position]
        else:
            tiles[position] = True
    sum = 0
    for x in tiles:
        if tiles[x]:
            sum += 1
    print(sum)