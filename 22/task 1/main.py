with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    player1Cards = []
    player2Cards = []
    isPlayer1Turn = True
    for line in input:
        if line == "" or line.find("Player 1:") != -1:
            continue
        if line.find("Player 2:") != -1:
            isPlayer1Turn = False
            continue
        number = int(line)
        if isPlayer1Turn:
            player1Cards.append(number)
        else:
            player2Cards.append(number)
    while len(player1Cards) > 0 and len(player2Cards) > 0:
        player1Card = player1Cards.pop(0)
        player2Card = player2Cards.pop(0)
        if player1Card > player2Card:
            player1Cards.append(player1Card)
            player1Cards.append(player2Card)
        else:
            player2Cards.append(player2Card)
            player2Cards.append(player1Card)
    winnerDeck = None
    if len(player1Cards) > 0:
        winnerDeck = player1Cards
    else:
        winnerDeck = player2Cards
    sum = 0
    for (x, i) in zip(winnerDeck, reversed(range(1, len(winnerDeck) + 1))):
        sum += x * i
    print(sum)