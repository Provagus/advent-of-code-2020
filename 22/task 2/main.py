def playGame(player1Cards : list, player2Cards : list):
    playedCombinations = []
    player1Cards = player1Cards.copy()
    player2Cards = player2Cards.copy()
    while len(player1Cards) > 0 and len(player2Cards) > 0:
        if [player1Cards, player2Cards] in playedCombinations:
            return (True, player1Cards, player2Cards)
        else:
            playedCombinations.append(
                [
                    player1Cards.copy(),
                    player2Cards.copy()
                ]
            )
        player1Card = player1Cards.pop(0)
        player2Card = player2Cards.pop(0)
        if len(player1Cards) >= player1Card and len(player2Cards) >= player2Card:
            (player1Won, deck1, deck2) = playGame(player1Cards[0:player1Card], player2Cards[0:player2Card])
            del deck1
            del deck2
            if player1Won:
                player1Cards.append(player1Card)
                player1Cards.append(player2Card)
            else:
                player2Cards.append(player2Card)
                player2Cards.append(player1Card)
        else:
            if player1Card > player2Card:
                player1Cards.append(player1Card)
                player1Cards.append(player2Card)
            else:
                player2Cards.append(player2Card)
                player2Cards.append(player1Card)
    if len(player1Cards) > 0:
        return (True, player1Cards, player2Cards)
    else:
        return (False, player1Cards, player2Cards)

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    player1Cards = []
    player2Cards = []
    isPlayer1Turn = True
    for line in input:
        if line == "" or line.find("Player 1:") != -1:
            continue
        if line.find("Player 2:") != -1:
            isPlayer1Turn = False
            continue
        number = int(line)
        if isPlayer1Turn:
            player1Cards.append(number)
        else:
            player2Cards.append(number)
    (player1Won, player1Cards, player2Cards) = playGame(player1Cards, player2Cards)
    winnerDeck = None
    if player1Won:
        winnerDeck = player1Cards
    else:
        winnerDeck = player2Cards
    sum = 0
    for (x, i) in zip(winnerDeck, reversed(range(1, len(winnerDeck) + 1))):
        sum += x * i
    print(sum)