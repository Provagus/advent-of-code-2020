import math
import copy

def rotateLeft(puzzle : list):
    i = len(puzzle) - 1
    rotated = []
    while i >= 0:
        newLine = []
        for x in puzzle:
            newLine.append(x[i])
        rotated.append(newLine)
        i -= 1
    return rotated

def upsideDown(puzzle : list):
    return rotateLeft(rotateLeft(puzzle))

def rotateRight(puzzle : list):
    return rotateLeft(upsideDown(puzzle))

def printPuzzleTile(puzzle : list):
    for line in puzzle:
        line = "".join(line)
        print(line)

def flipXAxis(puzzle : list):
    flipped = []
    for line in puzzle:
        flippedLine = line.copy()
        flippedLine.reverse()
        flipped.append(flippedLine)
    return flipped

def flipYAxis(puzzle : list):
    flipped = []
    for line in puzzle:
        newLine = line.copy()
        flipped.append(newLine)
    flipped.reverse()
    return flipped

def getBorders(puzzle : list):
    borders = {}
    borders["top"] = puzzle[0]
    borders["bottom"] = puzzle[-1]
    right = []
    left = []
    for x in puzzle:
        right.append(x[-1])
        left.append(x[0])
    borders["right"] = right
    borders["left"] = left
    return borders

def getTileMatchesTemlate():
    tileMatchesRotarionsPlaceholder = {
        "top" : [],
        "bottom" : [],
        "left" : [],
        "right" : []
    }
    tileMatchesPlaceholder = {
        "NN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "NFX" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "NFY" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "RN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "RFX" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "RFY" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "UN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "LN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
    }
    return tileMatchesPlaceholder

def forceMatching(fields : list, size : int, current : int, nextPuzzle : dict, matches : dict, used : dict):
    currentY = current // size
    currentX = current % size
    copyOfImage = copy.deepcopy(fields)
    if currentY >= size:
        return copyOfImage
    copyOfImage[currentY][currentX] = nextPuzzle
    if currentY > 0:
        above = fields[currentY - 1][currentX]
        tile = above["tile"]
        rotation = above["rotation"]
        if not nextPuzzle in matches[tile][rotation]["bottom"]:
            return None
    
    if current + 1 == size**2:
        return copyOfImage

    if currentX < size - 1:
        tile = nextPuzzle["tile"]
        rotation = nextPuzzle["rotation"]
        result = None
        for nextTile in matches[tile][rotation]["right"]:
            if nextTile["tile"] in used:
                continue
            newUsed = used.copy()
            newUsed[nextTile["tile"]] = True
            result = forceMatching(copyOfImage, size, current + 1, nextTile, matches, newUsed)
            if result:
                break
        return result

    else:
        tile = fields[currentY][0]["tile"]
        rotation = fields[currentY][0]["rotation"]
        result = None
        for nextTile in matches[tile][rotation]["bottom"]:
            if nextTile["tile"] in used:
                continue
            newUsed = used.copy()
            newUsed[nextTile["tile"]] = True
            result = forceMatching(copyOfImage, size, current + 1, nextTile, matches, newUsed)
            if result:
                break
        return result

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    tileNumber = None
    tiles = {}
    tileBorders = {}
    for line in input:
        if line.find("Tile") != -1:
            line = line.split(" ")
            line = line[1]
            line = line.replace(":", "")
            tileNumber = int(line)
            tiles[tileNumber] = []
        elif line == "":
            continue
        else:
            line = list(line)
            tiles[tileNumber].append(line)
    for x in tiles:
        tileBorders[x] = {
            "NN" : getBorders(tiles[x]),
            "NFX" : getBorders(flipXAxis(tiles[x])),
            "NFY" : getBorders(flipYAxis(tiles[x])),
            "RN" : getBorders(rotateRight(tiles[x])),
            "RFX" : getBorders(flipXAxis(rotateRight(tiles[x]))),
            "RFY" : getBorders(flipYAxis(rotateRight(tiles[x]))),
            "UN" : getBorders(upsideDown(tiles[x])),
            "LN" : getBorders(rotateLeft(tiles[x])),
        }

    opositeDirections = {
        "top" : "bottom",
        "bottom" : "top",
        "left" : "right",
        "right" : "left"
    }
    # Finding all matches
    matches ={}

    for x in tileBorders:
        matches[x] = getTileMatchesTemlate()
    for x in tileBorders:
        puzzle1 = tileBorders[x].copy()

        for y in tileBorders:
            if x == y:
                continue
            puzzle2 = tileBorders[y].copy()
            for dir1 in puzzle1:
                for dir2 in puzzle2:
                    for dir in opositeDirections:
                        if puzzle1[dir1][dir] == puzzle2[dir2][opositeDirections[dir]]:
                            #print(f"{x} {y} {dir1} {dir2} {dir} {opositeDirections[dir]}")
                            matches[x][dir1][dir].append(
                                {
                                    "rotation" : dir2,
                                    "tile" : y
                                }
                            )
    #print(matches)
    size = int(math.sqrt(len(tiles)))
    emptyArray = []
    for i in range(0, size):
        vertical = []
        for j in range(0, size):
            vertical.append([])
        emptyArray.append(vertical)
    result = None
    for tile in matches:
        for rotation in matches[tile]:
            startingTile = {
                "rotation" : rotation,
                "tile" : tile
            }
            #print(startingTile)
            result = forceMatching(emptyArray, size, 0, startingTile, matches, {})
            if result:
                #print(result)
                break
        if result:
            break
    image = []
    imageSize = len(tiles[result[0][0]["tile"]])
    for row in result:
        for i in range(1, imageSize - 1):
            line = []
            for tile in row:
                tileNumber = tile["tile"]
                tileRotation = tile["rotation"]
                tile = tiles[tileNumber]
                if tileRotation == "NFX":
                    tile = flipXAxis(tile)
                elif tileRotation == "NFY":
                    tile = flipYAxis(tile)
                elif tileRotation == "RN":
                    tile = rotateRight(tile)
                elif tileRotation == "RFX":
                    tile = flipXAxis(rotateRight(tile))
                elif tileRotation == "RFY":
                    tile = flipYAxis(rotateRight(tile))
                elif tileRotation == "UN":
                    tile = upsideDown(tile)
                elif tileRotation == "LN":
                    tile = rotateLeft(tile)
                line += tile[i][1:-1]
            image.append(line)

    monsterPicture = None
    with open("monster.in", "r") as file:
        monsterPicture = file.read()
        monsterPicture = monsterPicture.split("\n")
    monsterPoints = []
    for (x, i) in zip(monsterPicture, range(0, len(monsterPicture))):
        for j in range(0, len(x)):
            if x[j] == "#":
                monsterPoints.append([i, j])
    # Looking for sea monsters
    images = []
    images.append(image)
    images.append(flipXAxis(image))
    images.append(flipYAxis(image))
    images.append(rotateRight(image))
    images.append(flipXAxis(rotateRight(image)))
    images.append(flipYAxis(rotateRight(image)))
    images.append(upsideDown(image))
    images.append(rotateLeft(image))
    
    habitat = []
    for i in range(0, len(image)):
        line = []
        for j in range(0, len(image[0])):
            line.append(True)
        habitat.append(line)
    for image in images:
        monstersFound = 0
        freePlaces = 0
        
        for i in range(0, len(image)-18):
            for j in range(0, len(image[0])-2):
                isMonster = True
                for x in monsterPoints:
                    if image[j+x[0]][i+x[1]] != "#":
                        isMonster = False
                        break
                if isMonster:
                    monstersFound += 1
                    for x in monsterPoints:
                        habitat[j+x[0]][i+x[1]] = False
        if monstersFound:
            for i in range(0, len(image)):
                for j in range(0, len(image[0])):
                    if image[i][j] == "#" and habitat[i][j]:
                        freePlaces += 1
            print(freePlaces)
            break