import math
import copy

def rotateLeft(puzzle : list):
    i = len(puzzle) - 1
    rotated = []
    while i >= 0:
        newLine = []
        for x in puzzle:
            newLine.append(x[i])
        rotated.append(newLine)
        i -= 1
    return rotated

def upsideDown(puzzle : list):
    return rotateLeft(rotateLeft(puzzle))

def rotateRight(puzzle : list):
    return rotateLeft(upsideDown(puzzle))

def printPuzzleTile(puzzle : list):
    for line in puzzle:
        line = "".join(line)
        print(line)

def flipXAxis(puzzle : list):
    flipped = []
    for line in puzzle:
        flippedLine = line.copy()
        flippedLine.reverse()
        flipped.append(flippedLine)
    return flipped

def flipYAxis(puzzle : list):
    flipped = []
    for line in puzzle:
        newLine = line.copy()
        flipped.append(newLine)
    flipped.reverse()
    return flipped

def getBorders(puzzle : list):
    borders = {}
    borders["top"] = puzzle[0]
    borders["bottom"] = puzzle[-1]
    right = []
    left = []
    for x in puzzle:
        right.append(x[-1])
        left.append(x[0])
    borders["right"] = right
    borders["left"] = left
    return borders

def getTileMatchesTemlate():
    tileMatchesRotarionsPlaceholder = {
        "top" : [],
        "bottom" : [],
        "left" : [],
        "right" : []
    }
    tileMatchesPlaceholder = {
        "NN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "NFX" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "NFY" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "RN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "RFX" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "RFY" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "UN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
        "LN" : copy.deepcopy(tileMatchesRotarionsPlaceholder),
    }
    return tileMatchesPlaceholder

def forceMatching(fields : list, size : int, current : int, nextPuzzle : dict, matches : dict, used : dict):
    currentY = current // size
    currentX = current % size
    copyOfImage = copy.deepcopy(fields)
    if currentY >= size:
        return copyOfImage
    copyOfImage[currentY][currentX] = nextPuzzle
    if currentY > 0:
        above = fields[currentY - 1][currentX]
        tile = above["tile"]
        rotation = above["rotation"]
        if not nextPuzzle in matches[tile][rotation]["bottom"]:
            return None
    
    if current + 1 == size**2:
        return copyOfImage

    if currentX < size - 1:
        tile = nextPuzzle["tile"]
        rotation = nextPuzzle["rotation"]
        result = None
        for nextTile in matches[tile][rotation]["right"]:
            if nextTile["tile"] in used:
                continue
            newUsed = used.copy()
            newUsed[nextTile["tile"]] = True
            result = forceMatching(copyOfImage, size, current + 1, nextTile, matches, newUsed)
            if result:
                break
        return result

    else:
        tile = fields[currentY][0]["tile"]
        rotation = fields[currentY][0]["rotation"]
        result = None
        for nextTile in matches[tile][rotation]["bottom"]:
            if nextTile["tile"] in used:
                continue
            newUsed = used.copy()
            newUsed[nextTile["tile"]] = True
            result = forceMatching(copyOfImage, size, current + 1, nextTile, matches, newUsed)
            if result:
                break
        return result

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    tileNumber = None
    tiles = {}
    tileBorders = {}
    for line in input:
        if line.find("Tile") != -1:
            line = line.split(" ")
            line = line[1]
            line = line.replace(":", "")
            tileNumber = int(line)
            tiles[tileNumber] = []
        elif line == "":
            continue
        else:
            line = list(line)
            tiles[tileNumber].append(line)
    for x in tiles:
        tileBorders[x] = {
            "NN" : getBorders(tiles[x]),
            "NFX" : getBorders(flipXAxis(tiles[x])),
            "NFY" : getBorders(flipYAxis(tiles[x])),
            "RN" : getBorders(rotateRight(tiles[x])),
            "RFX" : getBorders(flipXAxis(rotateRight(tiles[x]))),
            "RFY" : getBorders(flipYAxis(rotateRight(tiles[x]))),
            "UN" : getBorders(upsideDown(tiles[x])),
            "LN" : getBorders(rotateLeft(tiles[x])),
        }

    opositeDirections = {
        "top" : "bottom",
        "bottom" : "top",
        "left" : "right",
        "right" : "left"
    }
    # Finding all matches
    matches ={}

    for x in tileBorders:
        matches[x] = getTileMatchesTemlate()
    for x in tileBorders:
        puzzle1 = tileBorders[x].copy()

        for y in tileBorders:
            if x == y:
                continue
            puzzle2 = tileBorders[y].copy()
            for dir1 in puzzle1:
                for dir2 in puzzle2:
                    for dir in opositeDirections:
                        if puzzle1[dir1][dir] == puzzle2[dir2][opositeDirections[dir]]:
                            #print(f"{x} {y} {dir1} {dir2} {dir} {opositeDirections[dir]}")
                            matches[x][dir1][dir].append(
                                {
                                    "rotation" : dir2,
                                    "tile" : y
                                }
                            )
    #print(matches)
    size = int(math.sqrt(len(tiles)))
    emptyArray = []
    for i in range(0, size):
        vertical = []
        for j in range(0, size):
            vertical.append([])
        emptyArray.append(vertical)
    result = None
    for tile in matches:
        for rotation in matches[tile]:
            startingTile = {
                "rotation" : rotation,
                "tile" : tile
            }
            #print(startingTile)
            result = forceMatching(emptyArray, size, 0, startingTile, matches, {})
            if result:
                #print(result)
                break
        if result:
            break
    answer = result[0][0]["tile"] * result[0][-1]["tile"] * result[-1][0]["tile"] * result[-1][-1]["tile"]
    print(answer)