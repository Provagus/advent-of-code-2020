def deep2LevelCopy(set : dict):
    newSet = {}
    for x in set:
        newSet[x] = {}
        for y in set[x]:
            newSet[x][y] = {}
            for z in set[x][y]:
                newSet[x][y][z] = set[x][y][z]
    return newSet

def countNeighbours(set, x, y, z):
    def isActive(x, y, z):
        active = False
        if x in set:
            if y in set[x]:
                if z in set[x][y]:
                    active = set[x][y][z]
        return active
    
    counter = 0
    for i in range(-1, 2):
        for j in range(-1, 2):
            for k in range(-1, 2):
                if i != 0 or j != 0 or k != 0:
                    if isActive(x+i, y+j, z+k):
                        counter += 1
    return counter

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    cordinates = {}
    cordinates[0] = {}
    for (x, i) in zip(input, range(0, len(input))):
        cordinates[0][i] = {}
        for j in range(0, len(x)):
            active = False
            if x[j] == "#":
                active = True
            cordinates[0][i][j] = active
    
    len1 = len(cordinates)
    len2 = len(cordinates[0])
    len3 = len(cordinates[0][0])
    for i in range(1, 7):
        newCordinates = deep2LevelCopy(cordinates)
        for j in range(-i, i+len1):
            if not j in newCordinates:
                newCordinates[j] = {}
            if not j in cordinates:
                cordinates[j] = {}

            for k in range(-i, i+len2):
                if not k in newCordinates[j]:
                    newCordinates[j][k] = {}
                if not k in cordinates[j]:
                    cordinates[j][k] = {}

                for l in range(-i, i+len3):
                    if not l in newCordinates[j][k]:
                        newCordinates[j][k][l] = False
                    if not l in cordinates[j][k]:
                        cordinates[j][k][l] = False
                    nbr = countNeighbours(cordinates, j, k, l)
                    if cordinates[j][k][l]:
                        if not (nbr == 2 or nbr == 3):
                            newCordinates[j][k][l] = False
                    else:
                        if nbr == 3:
                            newCordinates[j][k][l] = True
        cordinates = newCordinates

    sum = 0
    for x in cordinates:
        for y in cordinates[x]:
            for z in cordinates[x][y]:
                if cordinates[x][y][z]:
                    sum += 1
    print(sum)