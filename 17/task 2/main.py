def deep3LevelCopy(set : dict):
    newSet = {}
    for x in set:
        newSet[x] = {}
        for y in set[x]:
            newSet[x][y] = {}
            for z in set[x][y]:
                newSet[x][y][z] = {}
                for w in set[x][y][z]:
                    newSet[x][y][z][w] = set[x][y][z][w]
    return newSet

def countNeighbours(set, x, y, z, w):
    def isActive(x, y, z, w):
        active = False
        if x in set:
            if y in set[x]:
                if z in set[x][y]:
                    if w in set[x][y][z]:
                        active = set[x][y][z][w]
        return active
    
    counter = 0
    for i in range(-1, 2):
        for j in range(-1, 2):
            for k in range(-1, 2):
                for l in range(-1, 2):
                    if i != 0 or j != 0 or k != 0 or l != 0:
                        if isActive(x+i, y+j, z+k, w+l):
                            counter += 1
    return counter

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    cordinates = {}
    cordinates[0] = {}
    cordinates[0][0] = {}
    for (x, i) in zip(input, range(0, len(input))):
        cordinates[0][0][i] = {}
        for j in range(0, len(x)):
            active = False
            if x[j] == "#":
                active = True
            cordinates[0][0][i][j] = active
    
    len1 = len(cordinates[0])
    len2 = len(cordinates[0][0])
    len3 = len(cordinates[0][0][0])
    for i in range(1, 7):
        newCordinates = deep3LevelCopy(cordinates)
        for m in range(-i, i+len1):
            if not m in newCordinates:
                newCordinates[m] = {}
            if not m in cordinates:
                cordinates[m] = {}
            for j in range(-i, i+len1):
                if not j in newCordinates[m]:
                    newCordinates[m][j] = {}
                if not j in cordinates[m]:
                    cordinates[m][j] = {}

                for k in range(-i, i+len2):
                    if not k in newCordinates[m][j]:
                        newCordinates[m][j][k] = {}
                    if not k in cordinates[m][j]:
                        cordinates[m][j][k] = {}

                    for l in range(-i, i+len3):
                        if not l in newCordinates[m][j][k]:
                            newCordinates[m][j][k][l] = False
                        if not l in cordinates[m][j][k]:
                            cordinates[m][j][k][l] = False
                        nbr = countNeighbours(cordinates, m, j, k, l)
                        if cordinates[m][j][k][l]:
                            if not (nbr == 2 or nbr == 3):
                                newCordinates[m][j][k][l] = False
                        else:
                            if nbr == 3:
                                newCordinates[m][j][k][l] = True
        cordinates = newCordinates

    sum = 0
    for m in cordinates:
        for x in cordinates[m]:
            for y in cordinates[m][x]:
                for z in cordinates[m][x][y]:
                    if cordinates[m][x][y][z]:
                        sum += 1
    print(sum)