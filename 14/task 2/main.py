def analyze(i, mask, number, memory, current, value):
    if i >= len(number):
        position = "".join(current)
        position = int(position, base=2)
        memory[position] = value
        return
    if mask[i] == "1":
        current.append("1")
        analyze(i+1, mask, number, memory, current, value)
    elif mask[i] == "0":
        current.append(number[i])
        analyze(i+1, mask, number, memory, current, value)
    elif mask[i] == "X":
        current0 = current.copy()
        current0.append("0")
        analyze(i+1, mask, number, memory, current0, value)
        current1 = current.copy()
        current1.append("1")
        analyze(i+1, mask, number, memory, current1, value)

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    mask = ""
    memory = {}
    for x in input:
        x = x.split(" = ")
        if x[0] == "mask":
            mask = x[1]
        else:
            x[0] = x[0].replace("mem[", "").replace("]", "")
            position = int(x[0])
            number = int(x[1])
            position = format(position, '036b')
            analyze(0, mask, position, memory, [], number)
    sum = 0
    for x in memory:
        sum += memory[x]
    print(sum)
