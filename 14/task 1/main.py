from os import replace


with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    mask = ""
    memory = {}
    for x in input:
        x = x.split(" = ")
        if x[0] == "mask":
            mask = x[1]
        else:
            x[0] = x[0].replace("mem[", "").replace("]", "")
            position = int(x[0])
            number2 = int(x[1])
            number2 = format(number2, '036b')
            obtained = []
            for i in range(0, len(mask)):
                if mask[i] == "X":
                    obtained.append(number2[i])
                else:
                    obtained.append(mask[i])
            obtained = "".join(obtained)
            obtained = int(obtained, base=2)
            memory[position] = obtained
    sum = 0
    for x in memory:
        sum += memory[x]
    print(sum)
