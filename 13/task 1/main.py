with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    timestamp = int(input[0])
    busses = input[1].split(",")
    lowestWaitingTime = timestamp
    busID = 0
    for x in busses:
        if x == "x":
            continue
        x = int(x)
        waitingTime = x - (timestamp % x)
        if waitingTime < lowestWaitingTime:
            lowestWaitingTime = waitingTime
            busID = x
    print(busID * lowestWaitingTime)