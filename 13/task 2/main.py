primaries = { 1: False, 2: True, 3: True}

def isPrimary(a):
    if a in primaries:
        if primaries[a]:
            return True
        else:
            return False
    for x in range(2, (a//3)+1):
        if a % x == 0:
            primaries[a] = False
            return False
    primaries[a] = True
    return True

def primaryElements(a):
    elements = {}
    current = 2
    while True:
        if a == 1:
            elements[1] = 1
            break
        if not isPrimary(current):
            current += 1
            continue
        if a % current == 0:
            a /= current
            if current in elements:
                elements[current] += 1
            else:
                elements[current] = 1
        else:
            current += 1
    return elements

def lowestCommonMultiply(a, b):
    if a == 0 or b == 0:
        return 0
    elementsA = primaryElements(a)
    elementsB = primaryElements(b)
    answer = 1
    for x in elementsA:
        if x in elementsB:
            answer *= x**max(elementsA[x], elementsB[x])
        else:
            answer *= x**elementsA[x]
    for x in elementsB:
        if not x in elementsA:
            answer *= x**elementsB[x]
    return answer

with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    timestamp = int(input[0])
    busses = input[1].split(",")
    offset = -1
    increase = 1
    current = 0
    for x in busses:
        offset += 1
        if x == "x":
            continue
        x = int(x)
        if (current + offset) % x == 0:
            increase = lowestCommonMultiply(increase, x)
            continue
        else:
            while (current + offset) % x != 0:
                current += increase
            increase = lowestCommonMultiply(increase, x)
    print(current)