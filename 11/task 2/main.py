def isFirstOccupied(i, j, array, a, b):
    i += a
    j += b
    while 0 <= i < len(array) and 0 <= j < len(array[i]):
        if array[i][j] == "#":
            return True
        elif array[i][j] == "L":
            return False
        i += a
        j += b

with open("input.in", "r") as file:
    layout = file.read()
    layout = layout.split("\n")
    for i in range(0, len(layout)):
        layout[i] = list(layout[i])
    changes = 1
    while changes != 0:
        # Main processing
        newLayout = [x.copy() for x in layout]
        changes = 0
        for i in range(0, len(newLayout)):
            for j in range(0, len(newLayout[i])):
                if layout[i][j] == "L" or layout[i][j] == "#":
                    occupied = 0
                    if isFirstOccupied(i, j, layout, 0, 1):
                        occupied += 1
                    if isFirstOccupied(i, j, layout, 1, 0):
                        occupied += 1
                    if isFirstOccupied(i, j, layout, 1, 1):
                        occupied += 1
                    if isFirstOccupied(i, j, layout, 0, -1):
                        occupied += 1
                    if isFirstOccupied(i, j, layout, -1, 0):
                        occupied += 1
                    if isFirstOccupied(i, j, layout, -1, -1):
                        occupied += 1
                    if isFirstOccupied(i, j, layout, 1, -1):
                        occupied += 1
                    if isFirstOccupied(i, j, layout, -1, 1):
                        occupied += 1
                    if occupied == 0 and layout[i][j] == "L":
                        newLayout[i][j] = "#"
                        changes += 1
                    if occupied >= 5 and layout[i][j] == "#":
                        newLayout[i][j] = "L"
                        changes += 1
        layout = newLayout
    occupied = 0
    for x in layout:
        for y in x:
            if y == "#":
                occupied += 1
    print(occupied)