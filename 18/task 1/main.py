ADD = 0
MULTIPLY = 1

def analyzeEquation(input : list):
    parantheses = 0
    next = None
    sum = 0
    leftP = 0
    rightP = 0
    for (x, i) in zip(input, range(0, len(input))):
        if parantheses == 0:
            if x == "+":
                next = ADD
            elif x == "*":
                next = MULTIPLY
            elif x == "(":
                leftP = i
                parantheses += 1
            else:
                x = int(x)
                if next == ADD:
                    sum += x
                elif next == MULTIPLY:
                    sum *= x
                else:
                    sum = x
        else:
            if x == "(":
                parantheses += 1
            elif x == ")":
                parantheses -= 1
                if parantheses == 0:
                    rightP = i
                    result = analyzeEquation(input[leftP+1:rightP])
                    if next == ADD:
                        sum += result
                    elif next == MULTIPLY:
                        sum *= result
                    else:
                        sum = result
    return sum


with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    sum = 0
    for equation in input:
        equation = equation.replace("(", "( ").replace(")", " )")
        equation = equation.split(" ")
        sum += analyzeEquation(equation)
    print(sum)