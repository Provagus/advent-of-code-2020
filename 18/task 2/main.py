def analyzeEquation(input : list):
    parantheses = 0
    sum = 0
    leftP = 0
    rightP = 0
    i = 0
    while i < len(input):
        x = input[i]
        if parantheses == 0:
            if x == "(":
                leftP = i
                parantheses += 1
        else:
            if x == "(":
                parantheses += 1
            elif x == ")":
                parantheses -= 1
                if parantheses == 0:
                    rightP = i
                    result = analyzeEquation(input[leftP+1:rightP])
                    input = input[:leftP] + [result] + input[rightP+1:]
                    i = leftP
        i+= 1
    i = 0
    while i < len(input):
        x = input[i]
        if x == "+":
            result = int(input[i-1]) + int(input[i+1])
            input = input[:i-1] + [result] + input[i+2:]
            i =- 2
        i+= 1
    sum = 1
    for x in input:
        if x != "*":
            sum *= int(x)
    return sum


with open("input.in", "r") as file:
    input = file.read()
    input = input.split("\n")
    sum = 0
    for equation in input:
        equation = equation.replace("(", "( ").replace(")", " )")
        equation = equation.split(" ")
        sum += analyzeEquation(equation)
    print(sum)